import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts
import QtQuick.Dialogs

Page {
    font.pointSize: Qt.application.font.pointSize

    title: qsTr("Categories")

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            highlightMoveDuration: 0
            currentIndex: -1
            model: catsModel
            clip: true
            delegate: Text {
                text: index+1 + " - " + category
                MouseArea {
                    anchors.fill: parent
                    onClicked: listView.currentIndex = index
                }
            }
            highlight: Rectangle {
                color: 'grey'
            }
        }
        RowLayout {
            spacing: 5
            Button {
                id: btnAdd
                text: qsTr("Add")
                onClicked: {
                    stackView.push("AddCategory.qml");
                }
            }
            Button {
                id: btnRemove
                text: qsTr("Remove")
                enabled: listView.currentIndex >= 0
                onClicked: {
                    if ( listView.currentIndex >= 0 )
                    {
                        obj.categoryRemove( listView.currentIndex );
                        listView.currentIndex = -1;
                    }
                }
            }
        }
    }
}
