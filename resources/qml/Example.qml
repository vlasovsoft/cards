import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts

Page {
    property int id: 0
    property string propExample

    title: qsTr("Example")

    font.pointSize: Qt.application.font.pointSize
    ColumnLayout {
          anchors.fill: parent
          anchors.rightMargin: 5
          anchors.leftMargin: 5
          anchors.bottomMargin: 5
          anchors.topMargin: 5
          TextArea {
              id: example
              text: propExample
              Layout.fillWidth: true
              Layout.fillHeight: true
          }
          RowLayout {
              Button {
                  text: id > 0 ? qsTr("Update") : qsTr("Add")
                  enabled: example.text.length > 0
                  onClicked: {
                      stackView.pop();
                      if ( id > 0 )
                          obj.exampleUpdate( id, example.text );
                      else
                          obj.exampleAdd( example.text );
                  }
              }
          }
    }
}
