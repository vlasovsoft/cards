import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import com.vlasovsoft.downloader 1.0

Page {
    property int id: 0

    Downloader {
        id: downloader

        onMessage: function(txt) {
            text.append(txt);
        }
    }

    font.pointSize: Qt.application.font.pointSize
    ColumnLayout {
          anchors.fill: parent
          anchors.rightMargin: 5
          anchors.leftMargin: 5
          anchors.bottomMargin: 5
          anchors.topMargin: 5
          TextArea {
              id: text
              readOnly: true
              Layout.fillHeight: true
              Layout.fillWidth: true
          }
    }

    Component.onCompleted: {
        downloader.download();
    }
}
