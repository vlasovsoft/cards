import QtQuick 2.9
import QtQuick.Controls 2.2
import com.vlasovsoft.myobject 1.0

ApplicationWindow {
    id: window
    visible: true
    visibility: "Maximized"
    width: 640
    height: 480
    title: qsTr("Cards")

    MyObject {
         id: obj;

         onCurrentCardUpdated: {
             menuEditCard.visible = menuDeleteCard.visible = obj.currId() > 0 && obj.currChecked();
             lesson.update();
         }
    }

    onClosing: function(close) {
        if ( drawer.visible )
        {
            drawer.close();
            close.accepted = false;
        }
        else
        if (stackView.depth > 1) {
            stackView.pop()
            close.accepted = false;
        }
    }

    header: ToolBar {
        contentHeight: btnMenu.implicitHeight

        ToolButton {
            id: btnMenu
            visible: stackView.depth <= 1
            Image {
                source: "qrc:/images/menu.svg"
                anchors.fill: parent
                anchors.margins: 5
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                drawer.open()
            }
        }

        ToolButton {
              id: btnBack
              visible: stackView.depth > 1
              Image {
                  source: "qrc:/images/back.svg"
                  anchors.fill: parent
                  anchors.margins: 5
                  fillMode: Image.PreserveAspectFit
              }
              onClicked: {
                  stackView.pop()
              }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
            font.pointSize: 0.8*Qt.application.font.pointSize;
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.5
        height: window.height
        font.pointSize: 1.0*Qt.application.font.pointSize

        Column {
            anchors.fill: parent
            ItemDelegate {
                text: qsTr("Cards")
                width: parent.width
                onClicked: {
                    stackView.push("Cards.qml")
                    drawer.close()
                }
            }            
            ItemDelegate {
                id: menuNewCard
                text: qsTr("New card")
                width: parent.width
                onClicked: {
                    lesson.newCard()
                    drawer.close()
                }
            }
            ItemDelegate {
                id: menuEditCard
                text: qsTr("Edit card")
                width: parent.width
                onClicked: {
                    lesson.editCard()
                    drawer.close()
                }
            }
            ItemDelegate {
                id: menuDeleteCard
                text: qsTr("Delete card")
                width: parent.width
                onClicked: {
                    lesson.deleteCard()
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Download")
                width: parent.width
                onClicked: {
                    stackView.push("Download.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("Settings.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: Lesson {
            id: lesson
        }
        anchors.fill: parent
    }
}
