import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts

Page {
    property int id: 0

    font.pointSize: Qt.application.font.pointSize
    ColumnLayout {
          anchors.fill: parent
          anchors.rightMargin: 5
          anchors.leftMargin: 5
          anchors.bottomMargin: 5
          anchors.topMargin: 5
          Label {
              text: qsTr("Phrase")
          }
          TextField {
              id: phrase
              Layout.fillWidth: true
          }
          Label {
              text: qsTr("Phonetic")
          }
          TextField {
              id: phonetic
              Layout.fillWidth: true
          }
          Label {
              text: qsTr("Translation")
          }
          TextField {
              id: translation
              Layout.fillWidth: true
          }
          Item {
              Layout.fillHeight: true
          }
          RowLayout {
              Button {
                  text: id > 0 ? qsTr("Update") : qsTr("Add")
                  enabled: phrase.text.length > 0 && translation.text.length > 0
                  onClicked: {
                      stackView.pop();
                      if ( id > 0 )
                          obj.cardUpdate( id, phrase.text, translation.text, phonetic.text );
                      else
                          obj.cardAdd( phrase.text, translation.text, phonetic.text );
                  }
              }
          }
    }

    Component.onCompleted: {
        if ( id > 0 ) {
            phrase.text = obj.currPhrase();
            translation.text = obj.currTrans();
            phonetic.text = obj.currPhonetic();
        }
    }
}
