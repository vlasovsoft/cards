import QtQuick 2.2

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts

Page {
    id: page
    title: qsTr("Lesson")

    font.pointSize: Qt.application.font.pointSize

    ColumnLayout {
        id: column
        spacing: 0
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 0
        anchors.topMargin: 5
        anchors.fill: parent

        RowLayout {
            id: rowUp
            spacing: 5
            Button {
                id: btnSound
                icon.source: "qrc:/images/audio.svg"
                icon.width: 50
                icon.height: 50
                Layout.fillWidth: true
                onClicked: {
                    play();
                }
            }
            Button {
                id: btnExamples
                icon.source: "qrc:/images/list.svg"
                icon.width: 50
                icon.height: 50
                Layout.fillWidth: true
                onClicked: {
                    stackView.push("Examples.qml");
                }
            }
        }

        Label {
            id: timestamp
            font.pointSize: Qt.application.font.pointSize
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }

        Item {
            Layout.fillHeight: true
        }

        Label {
            id: phrase
            font.pointSize: 2 * Qt.application.font.pointSize
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }
        Label {
            id: phonetic
            font.pointSize: Qt.application.font.pointSize
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }
        Label {
            id: trans
            font.pointSize: 2 * Qt.application.font.pointSize
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }
        Label {
            id: example
            font.pointSize: Qt.application.font.pointSize
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
        }

        Rectangle {
            color: "red";
            Layout.fillHeight: true
        }

        RowLayout {
            id: rowDown
            spacing: 5
            Button {
                id: btnCheck
                Image {
                    fillMode: Image.PreserveAspectFit
                    anchors.centerIn: parent
                    sourceSize.height: btnCheck.background.height
                    height: sourceSize.height
                    source: "qrc:/images/eye.svg"
                }
                Layout.fillWidth: true
                implicitHeight: 100
                onClicked: {
                    check();
                }
            }
            Button {
                id: btnForget
                Image {
                    fillMode: Image.PreserveAspectFit
                    anchors.centerIn: parent
                    sourceSize.height: btnForget.background.height
                    height: sourceSize.height
                    source: "qrc:/images/sad.svg"
                }
                Layout.fillWidth: true
                implicitHeight: 100
                onClicked: {
                    answer(false);
                    obj.cardNext();
                }
            }
            Button {
                id: btnRemember
                Image {
                    fillMode: Image.PreserveAspectFit
                    anchors.centerIn: parent
                    sourceSize.height: btnRemember.background.height
                    height: sourceSize.height
                    source: "qrc:/images/smile.svg"
                }
                Layout.fillWidth: true
                implicitHeight: 100
                onClicked: {
                    answer(true);
                    obj.cardNext();
                }
            }
        }
    }

    Component.onCompleted: {
        obj.cardNext();
    }

    function answer(a)
    {
        obj.cardUpdate(a);        
    }

    function play()
    {
        obj.cardSpeakPhrase();
    }

    function check()
    {        
        obj.currCheck();
        play();
    }

    function update()
    {
        var id = obj.currId();
        // texts
        phrase.text = id > 0 ? obj.currPhrase() : qsTr("No cards found!");
        trans.text = obj.currTrans();
        phonetic.text = obj.currPhonetic();
        example.text = obj.currExample();
        timestamp.text = obj.currTimestamp();
        // visibility
        phrase.visible = id >= 0 && (!obj.currReversed() || obj.currChecked());
        trans.visible  = id >  0 && (obj.currReversed()  || obj.currChecked());
        phonetic.visible = id > 0 && obj.currChecked() && phonetic.text.length > 0;
        example.visible = id > 0 && obj.currChecked() && example.text.length > 0;
        btnCheck.visible = id > 0 && !obj.currChecked();
        btnForget.visible = btnRemember.visible = id > 0 && obj.currChecked();
        btnSound.enabled = obj.currMediaExists();
        rowUp.visible = id > 0 && obj.currChecked();
        rowDown.visible = id > 0;
    }

    function newCard() {
        stackView.push("Card.qml");
    }

    function editCard() {
        stackView.push("Card.qml", {id: obj.currId()});
    }

    function deleteCard() {
        obj.cardRemove();
    }
}

