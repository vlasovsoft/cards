import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    property int id: 0

    font.pointSize: Qt.application.font.pointSize
    ColumnLayout {
          anchors.fill: parent
          anchors.rightMargin: 5
          anchors.leftMargin: 5
          anchors.bottomMargin: 5
          anchors.topMargin: 5
          GroupBox {
              title: qsTr("Mode")
              Layout.fillWidth: true
              ColumnLayout {
                  RadioButton {
                      checked: 0 === obj.mode
                      text: qsTr("All phrases")
                      onToggled:
                      {
                          if (checked)
                              obj.mode = 0
                      }
                  }
                  RadioButton {
                      checked: 1 === obj.mode
                      text: qsTr("Drill the worst 10")
                      onToggled:
                      {
                          if (checked)
                              obj.mode = 1
                      }
                  }
              }
          }
          Item {
              Layout.fillHeight: true
              Layout.fillWidth: true
          }
    }
}
