import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.1

Page {
    property int cardId

    font.pointSize: Qt.application.font.pointSize

    title: qsTr("Set category")

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        ListView {
            property bool ready: false

            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            highlightMoveDuration: 0
            model: catsModel
            clip: true
            delegate: Text {
                text: index+1 + " - " + category
                MouseArea {
                    anchors.fill: parent
                    onClicked: listView.currentIndex = index
                }
            }
            highlight: Rectangle {
                color: 'grey'
            }
        }
        RowLayout {
            spacing: 5
            Button {
                id: btnAdd
                text: qsTr("Set")
                onClicked: {
                    obj.cardUpdateCategory(cardId, listView.currentIndex);
                    obj.refreshCardsModel();
                    stackView.pop();
                }
            }
        }
    }
}
