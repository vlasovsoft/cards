import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts
import QtQuick.Dialogs

Page {
    property bool initDone: false
    property alias currItem: listView.currentItem

    font.pointSize: Qt.application.font.pointSize

    title: qsTr("Cards")

    Component.onCompleted: {
        initDone = true;
        cardsModel.updateModel();
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        TextField {
            id: filter
            Layout.fillWidth: true
            visible: false
            onTextChanged: {
                obj.setCardsModelFilter( text );
                obj.refreshCardsModel();
            }
        }
        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            highlightMoveDuration: 0
            currentIndex: -1
            model: cardsModel
            clip: true
            delegate: Text {
                text: getText()
                color: "lightgray"
                MouseArea {
                    anchors.fill: parent
                    onClicked: listView.currentIndex = listView.currentIndex != index ? index : -1
                }
                function getText() {
                    var result = index+1 + ") " + phrase;
                    if ( trans )
                        result += " - " + trans;
                    if ( phonetic )
                        result += " - " + phonetic;
                    return result;
                }
            }
            highlight: Rectangle {
                color: 'grey'
            }
        }
        RowLayout {
            spacing: 5
            Button {
                id: btnOpen
                text: qsTr("Open")
                visible: listView.currentIndex >= 0
                onClicked: {
                    if ( listView.currentIndex >= 0 )
                    {
                        obj.cardOpen( cardsModel.getId(listView.currentIndex) );
                        listView.currentIndex = -1;
                        stackView.pop();
                    }
                }
            }
            Button {
                id: btnFilter
                text: qsTr("Filter")
                onClicked: {
                    filter.visible = !filter.visible;
                }
            }
        }
    }
}
