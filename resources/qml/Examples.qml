import QtQuick 2.2

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts

Page {
    id: page
    title: qsTr("Examples")

    font.pointSize: Qt.application.font.pointSize

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            highlightMoveDuration: 0
            currentIndex: -1
            model: examplesModel
            clip: true
            delegate: Text {
                text: index+1 + ". " + modeltext
                color: "lightgray"
                wrapMode: Text.WordWrap
                MouseArea {
                    anchors.fill: parent
                    onClicked: listView.currentIndex = index
                }
            }
            highlight: Rectangle {
                color: 'grey'
            }
        }
        RowLayout {
            spacing: 5
            Button {
                id: btnAdd
                text: qsTr("Add")
                onClicked: {
                    stackView.push("Example.qml");
                }
            }
            Button {
                id: btnRemove
                text: qsTr("Remove")
                enabled: listView.currentIndex >= 0
                onClicked: {
                    if ( listView.currentIndex >= 0 )
                    {
                        obj.exampleRemove( examplesModel.getId(listView.currentIndex) );
                        obj.refreshExamplesModel();
                        listView.currentIndex = -1;
                    }
                }
            }
            Button {
                id: btnEdit
                text: qsTr("Edit")
                enabled: listView.currentIndex >= 0
                onClicked: {
                    if ( listView.currentIndex >= 0 )
                    {
                        stackView.push("Example.qml",
                           { id: examplesModel.getId(listView.currentIndex),
                             propExample: examplesModel.getExample(listView.currentIndex)});
                        listView.currentIndex = -1;
                    }
                }
            }
        }
    }
}

