import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts

Page {
    font.pointSize: Qt.application.font.pointSize
    ColumnLayout {
          anchors.fill: parent
          anchors.rightMargin: 5
          anchors.leftMargin: 5
          anchors.bottomMargin: 5
          anchors.topMargin: 5
          Label {
              text: qsTr("Category")
          }
          TextField {
              id: category
              Layout.fillWidth: true
          }
          Item {
              Layout.fillHeight: true
          }
          RowLayout {
              Button {
                  text: "Add"
                  onClicked: {
                      stackView.pop();
                      if ( category.text.length > 0 )
                      {
                          obj.categoryAdd( category.text );
                      }
                  }
              }
              Button {
                  text: "Cancel"
                  onClicked: stackView.pop()
              }
          }
    }
}
