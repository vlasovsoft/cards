<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>Cards</name>
    <message>
        <location filename="../Cards.qml" line="7"/>
        <location filename="../Cards.qml" line="12"/>
        <source>Cards</source>
        <translation>Карточки</translation>
    </message>
    <message>
        <location filename="../Cards.qml" line="8"/>
        <source>Card is added</source>
        <translation>Карточка добавлена</translation>
    </message>
    <message>
        <location filename="../Cards.qml" line="13"/>
        <source>Card is removed</source>
        <translation>Карточка удалена</translation>
    </message>
</context>
<context>
    <name>CardsForm.ui</name>
    <message>
        <location filename="../CardsForm.ui.qml" line="13"/>
        <source>Cards</source>
        <translation>Карточки</translation>
    </message>
    <message>
        <location filename="../CardsForm.ui.qml" line="24"/>
        <source>Phrase</source>
        <translation>Фраза</translation>
    </message>
    <message>
        <location filename="../CardsForm.ui.qml" line="32"/>
        <source>Translation</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="../CardsForm.ui.qml" line="43"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../CardsForm.ui.qml" line="47"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../CardsForm.ui.qml" line="52"/>
        <source>Forget all</source>
        <translation>Забыть все</translation>
    </message>
</context>
<context>
    <name>Lesson</name>
    <message>
        <location filename="../Lesson.qml" line="35"/>
        <source>Lesson is over!</source>
        <translation>Урок окончен!</translation>
    </message>
</context>
<context>
    <name>LessonForm.ui</name>
    <message>
        <location filename="../LessonForm.ui.qml" line="13"/>
        <source>Lesson</source>
        <translation>Урок</translation>
    </message>
    <message>
        <location filename="../LessonForm.ui.qml" line="57"/>
        <source>Forget</source>
        <translation>Забыл</translation>
    </message>
    <message>
        <location filename="../LessonForm.ui.qml" line="63"/>
        <source>Remember</source>
        <translation>Помню</translation>
    </message>
    <message>
        <source>Repeat</source>
        <translation type="vanished">Повтор</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <location filename="../LessonForm.ui.qml" line="69"/>
        <source>Next</source>
        <translation>Дальше</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Stack</source>
        <translation type="vanished">Стек</translation>
    </message>
    <message>
        <location filename="../main.qml" line="10"/>
        <location filename="../main.qml" line="64"/>
        <source>Cards</source>
        <translation>Карточки</translation>
    </message>
    <message>
        <location filename="../main.qml" line="72"/>
        <source>Categories</source>
        <translation>Категории</translation>
    </message>
</context>
</TS>
