#ifndef CATSLISTMODEL_H
#define CATSLISTMODEL_H

#include <QObject>
#include <QSqlQueryModel>

class CatsListModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,      // id
        CategoryRole,                   // Category
    };

    explicit CatsListModel(QObject *parent = 0);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;

signals:

public slots:
    void updateModel();
    int getId(int row);
};

#endif // CATSLISTMODEL_H
