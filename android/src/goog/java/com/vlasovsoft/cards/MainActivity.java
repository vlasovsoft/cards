package com.vlasovsoft.cards;

import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import android.media.AudioManager;

public class MainActivity extends org.qtproject.qt5.android.bindings.QtActivity {

    private static MainActivity me;

    public MainActivity()
    {
        me = this;
    }

    @Override
    public void onCreateHook(Bundle savedInstanceState) {
        ENVIRONMENT_VARIABLES += "\tDB_PATH=" + getExternalFilesDir(null);
        super.onCreateHook(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }
}
