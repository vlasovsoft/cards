#!/bin/bash
set -e
app=cards
ndkVer=21.4.7075529
export ANDROID_SDK_ROOT=$HOME/apps/Android/Sdk
export ANDROID_NDK_ROOT=$HOME/apps/Android/Sdk/ndk/$ndkVer
ROOT=$HOME/workspace/$app
BUILD=$HOME/workspace/build/android/$app
KEYSTORE=$ROOT/android/keystore

ver=`cat $ROOT/VERSION`

echo "Checking data consistency"

mkdir -p $BUILD/$ver
pushd $BUILD/$ver

qtver=5.15.2
minSdk=21
targetSdk=30
compileSdk=30
abis="armeabi-v7a arm64-v8a x86 x86_64"

export QT=/home/sergv/apps/Qt/$qtver/android

cmake \
-DCMAKE_BUILD_TYPE=Release \
-DANDROID_ABI:STRING=armeabi-v7a \
-DANDROID_BUILD_ABI_arm64-v8a:BOOLEAN=ON \
-DANDROID_BUILD_ABI_armeabi-v7a:BOOLEAN=ON \
-DANDROID_BUILD_ABI_x86:BOOLEAN=ON \
-DANDROID_BUILD_ABI_x86_64:BOOLEAN=ON \
-DANDROID_PACKAGE_SOURCE_DIR:STRING=$ROOT/android \
-DANDROID_NATIVE_API_LEVEL:STRING=$minSdk \
-DANDROID_NDK:PATH=$ANDROID_NDK_ROOT \
-DANDROID_SDK:PATH=$ANDROID_SDK_ROOT \
-DANDROID_STL:STRING=c++_shared \
-DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM:STRING=BOTH \
-DCMAKE_PREFIX_PATH:STRING=$QT \
-DCMAKE_TOOLCHAIN_FILE:PATH=$ANDROID_NDK_ROOT/build/cmake/android.toolchain.cmake \
$ROOT

cmake --build .

mkdir -p android-build/src/qt
cp -r $QT/src/android/java/* android-build/src/qt
cp -r $QT/src/android/templates/res android-build
cp -r $ROOT/android/src android-build
cp -r $ROOT/android/gradle android-build
cp $ROOT/android/build.gradle android-build
cp $ROOT/android/gradlew android-build
cp $ROOT/android/local.properties android-build
cp $ROOT/android/AndroidManifest.xml android-build
cp $ROOT/android/keystore android-build
mkdir -p android-build/assets
cp -r i18n android-build/assets

$QT/bin/androiddeployqt --verbose --aux-mode --input `pwd`/android_deployment_settings.json --output `pwd`/android-build

for abi in $abis
do
  cp $ANDROID_NDK_ROOT/sources/cxx-stl/llvm-libc++/libs/$abi/libc++_shared.so android-build/libs/$abi
done

cp android-build/res/values/libs.xml android-build/src/main/res/values

# create gradle.properties
echo "VERSION=$ver" > ./android-build/gradle.properties
echo "MIN_SDK=$minSdk" >> ./android-build/gradle.properties
echo "TARGET_SDK=$targetSdk" >> ./android-build/gradle.properties
echo "COMPILE_SDK=$compileSdk" >> ./android-build/gradle.properties
echo "NDK_VER=$ndkVer" >> ./android-build/gradle.properties

popd # ver

notify-send $ver 'Build finished!' -i face-smile

