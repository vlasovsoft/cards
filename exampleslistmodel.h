#ifndef EXAMPLESLISTMODEL_H
#define EXAMPLESLISTMODEL_H

#include <QObject>
#include <QSqlQueryModel>

class ExamplesListModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,  // id
        TextRole,                   // Category
    };

    explicit ExamplesListModel(QObject *parent = 0);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;

signals:

public slots:
    void updateModel(int cardId);
    int getId(int row);

public:
    Q_INVOKABLE QString getExample(int row) const;
};

#endif // EXAMPLESLISTMODEL_H

