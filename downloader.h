#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QQueue>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class Downloader : public QObject
{
    Q_OBJECT

    QNetworkAccessManager* manager;

    QQueue<QString> phrases;

public:
    Downloader(QObject *parent = nullptr);
    ~Downloader();

    Q_INVOKABLE void download();

signals:
    void message(QString txt);

private slots:
    void htmlFinished();
    void mp3ReadyRead();
    void mp3Finished();
    void sslErrors(const QList<QSslError> &errors);
    void errorOccurred(QNetworkReply::NetworkError);

private:
    void step();
    QString getFileName() const;
    QString getTempFileName() const;

    static QString get_file_name(const QString& phrase);
};

#endif // DOWNLOADER_H
