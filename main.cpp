#include <QDir>
#include <QFont>
#include <QTranslator>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSqlDatabase>
#include <QStandardPaths>
#include <QSettings>

#include <QDebug>

#include "db.h"
#include "utils.h"
#include "myobject.h"
#include "downloader.h"
#include "server.h"
#include "cardslistmodel.h"
#include "exampleslistmodel.h"

CardsListModel* modelCards;
ExamplesListModel* modelExamples;

QSettings* ini;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QFont f(app.font());
    f.setPointSize(20);
    app.setFont(f);

    app.setOrganizationName("vlasovsoft");
    app.setApplicationName("cards");

    QTranslator translator;
    if (translator.load( get_root() + QDir::separator() + "i18n" + QDir::separator() + get_lang() + ".qm" ))
    {
        app.installTranslator(&translator);
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    if ( !initDatabase( db ) )
        return 1;

    qmlRegisterType<MyObject>("com.vlasovsoft.myobject", 1, 0, "MyObject");
    qmlRegisterType<Downloader>("com.vlasovsoft.downloader", 1, 0, "Downloader");
    qmlRegisterType<Server>("com.vlasovsoft.server", 1, 0, "Server");

    QQmlApplicationEngine engine;

    ini = new QSettings( app.organizationName(), app.applicationName(), &app );

    engine.rootContext()->setContextProperty( "examplesModel", modelExamples = new ExamplesListModel( &app ) );
    engine.rootContext()->setContextProperty( "cardsModel", modelCards = new CardsListModel( &app ) );

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
