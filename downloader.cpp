#include <QSqlQuery>
#include <QSqlError>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QFile>
#include <QDir>

#include "utils.h"

#include "downloader.h"

Downloader::Downloader(QObject *parent)
    : QObject(parent)
    , manager(new QNetworkAccessManager(this))
{
}

Downloader::~Downloader()
{
}

void Downloader::download()
{
    QSqlQuery q;
    q.prepare("SELECT phrase FROM cards");
    if ( q.exec() )
    {
        while ( q.next() )
        {
            QString phrase(q.value("phrase").toString());
            QFile file(get_file_name(phrase));
            if (!file.exists() && !phrase.contains(' ')) {
                phrases.enqueue(phrase);
            }
        }
    }
    else
    {
        qDebug() << q.lastError();
    }

    step();
}

void Downloader::htmlFinished()
{
    qDebug("Html finished!");
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if ( reply )
    {
        QString html = reply->readAll();
        QRegularExpression re("<source type=\"audio\\/mpeg\" src=\"([_a-zA-Z0-9\\.\\/]+\\.mp3)\"");
        QRegularExpressionMatch match = re.match(html);
        if ( match.hasMatch() )
        {
            QString matched = match.captured(1);
            QNetworkReply* reply = manager->get(QNetworkRequest(QUrl(QString("https://dictionary.cambridge.org") + matched)));
            QObject::connect(reply, SIGNAL(readyRead()), this, SLOT(mp3ReadyRead()));
            QObject::connect(reply, SIGNAL(finished()), this, SLOT(mp3Finished()));
            QFile::remove(getTempFileName());
        }
        else
        {
            QFile file(getTempFileName());
            if ( file.open(QIODevice::WriteOnly) )
            {
                file.write(html.toLocal8Bit());
                file.close();
            }
            qDebug("Missing mp3 link!");
        }
    }
}

void Downloader::mp3ReadyRead()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QFile file(getTempFileName());
    if ( file.open( QIODevice::Append )) {
        file.write(reply->readAll());
    }
    qDebug() << "mp3ReadyRead()";
}

void Downloader::mp3Finished()
{
    QFile file(getTempFileName());
    if (!file.rename(getFileName())) {
        file.remove();
    }
    emit message(phrases.dequeue());
    step();
}

void Downloader::sslErrors(const QList<QSslError> &errors)
{
    foreach( QSslError error, errors ) {
        qDebug() << "SSL error: " << error;
    }
}

void Downloader::errorOccurred(QNetworkReply::NetworkError e)
{
    qDebug() << "Network error: " << e;
}

void Downloader::step()
{
    if ( !phrases.isEmpty() ) {
        QNetworkReply* reply = manager->get(QNetworkRequest(QUrl(QString("https://dictionary.cambridge.org/dictionary/english/") + phrases.head())));
        QObject::connect(reply, SIGNAL(finished()), this, SLOT(htmlFinished()));
        QObject::connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
        QObject::connect(reply, SIGNAL(errorOccurred(QNetworkReply::NetworkError)), this, SLOT(errorOccurred(QNetworkReply::NetworkError)));
    } else {
        emit message("Download finished");
    }
}

QString Downloader::getFileName() const
{
    return get_file_name(phrases.head());
}

QString Downloader::getTempFileName() const
{
    return getFileName() + ".tmp";
}

QString Downloader::get_file_name(const QString &phrase)
{
    return get_data_path() + QDir::separator() + phrase + ".mp3";
}
