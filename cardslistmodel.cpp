#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

#include "settings.h"

#include "cardslistmodel.h"
#include "catslistmodel.h"

extern CatsListModel* modelCats;

CardsListModel::CardsListModel(QObject *parent)
    : QSqlQueryModel(parent)
{
    updateModel();
}

QVariant CardsListModel::data(const QModelIndex &index, int role) const
{
    // Define the column number, on the role of number
    int columnId = role - Qt::UserRole - 1;

    // Create the index using a column ID
    QModelIndex modelIndex = this->index(index.row(), columnId);

    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

QHash<int, QByteArray> CardsListModel::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[PhraseRole] = "phrase";
    roles[TransRole] = "trans";
    roles[PhoneticRole] = "phonetic";
    roles[DeltaRole] = "delta";
    return roles;
}

void CardsListModel::updateModel()
{
    setQuery( QString("SELECT id, phrase, trans, phonetic, timedelta FROM cards WHERE ( phrase LIKE '%2' OR trans LIKE '%2' ) ORDER BY timedelta " ).arg(filter.isEmpty()? "%" : QString("%")+filter+"%"));
}

int CardsListModel::getId(int row)
{
    return this->data(this->index(row, 0), IdRole).toInt();
}

QString CardsListModel::getPhrase(int row) const
{
    return data(index(row,0), PhraseRole).toString();
}

QString CardsListModel::getTrans(int row) const
{
    return data(index(row,0), TransRole).toString();
}

QString CardsListModel::getPhonetic(int row) const
{
    return data(index(row,0), PhoneticRole).toString();
}
