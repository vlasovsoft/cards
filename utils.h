#ifndef UTILS_H
#define UTILS_H

#include <QString>

QString get_root();
QString get_qml_root();
QString get_data_path();
QString get_lang();
int rand(int min, int max);

#endif // UTILS_H
