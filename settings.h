#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

extern QSettings* ini;

inline int settings_get_mode() {
    return ini->value("mode", 0).toInt();
}

inline void settings_set_mode(int mode) {
    ini->setValue("mode", mode);
}

#endif // SETTINGS_H
