#include <QDir>
#include <QStandardPaths>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QTime>
#include <QGuiApplication>
#include <QAudioOutput>
#include <QRandomGenerator>

#if defined(ANDROID)
#include <QAndroidJniObject>
#endif

#include <QDebug>

#include "db.h"
#include "utils.h"
#include "settings.h"
#include "cardslistmodel.h"
#include "exampleslistmodel.h"

#include "myobject.h"

extern CardsListModel* modelCards;
extern ExamplesListModel* modelExamples;

MyObject::MyObject(QObject *parent)
    : QObject(parent)
    , player(new QMediaPlayer(this))
    , card_id(0)
    , reversed(false)
    , timedelta(0)
{
    srand(0);
    player->setAudioOutput(new QAudioOutput(player));
    QObject::connect( qApp, SIGNAL(applicationStateChanged(Qt::ApplicationState)), this, SLOT(stateChanged(Qt::ApplicationState)) );
}

int MyObject::getMode() const
{
    return settings_get_mode();
}

void MyObject::setMode(int val)
{
    settings_set_mode(val);
    emit modeChanged();
}

int MyObject::currId() const
{
    return card_id;
}

QString MyObject::currPhrase() const
{
    return phrase;
}

QString MyObject::currTrans() const
{
    return trans;
}

QString MyObject::currPhonetic() const
{
    return phonetic;
}

QString MyObject::currExample() const
{
    return example;
}

QString MyObject::currTimestamp() const
{
    return timestamp;
}

bool MyObject::currReversed() const
{
    return reversed;
}

bool MyObject::currChecked() const
{
    return checked;
}

bool MyObject::currMediaExists() const
{
    return QFile::exists(::get_data_path() + QDir::separator() + phrase + ".mp3");
}

void MyObject::currCheck()
{
    checked = true;
    emit currentCardUpdated();
}

void MyObject::cardRemove()
{
    QSqlQuery q;
    q.prepare("DELETE FROM cards WHERE id=:id");
    q.bindValue(":id", card_id);
    if ( q.exec() )
    {
        refreshCardsModel();
        cardNext();
    }
    else
    {
        qDebug() << q.lastError();
    }
}

void MyObject::cardUpdate( bool success )
{
    if (0 == getMode())
    {
        timedelta = success ? qMin(3600*24*30, timedelta * 2) : qMax(60, timedelta / 10);
    }
    else
    {
        // timedelta = success ? qMin(3600*24*30, timedelta * 11 / 10) : qMax(60, timedelta * 9 / 10);
    }
    if (0 == getMode())
    {
        QSqlQuery q;
        if ( !q.exec(QString("UPDATE cards SET timedelta = '%1', timestamp = datetime('now','localtime','+%1 seconds') WHERE id = %2").arg(timedelta).arg(card_id)) )
        {
            qDebug() << q.lastError();
        }
    }
}

bool MyObject::cardOpen(int id)
{
    bool result = false;

    phrase.clear();
    trans.clear();
    phonetic.clear();
    example.clear();

    QSqlQuery q;
    q.prepare( "SELECT phrase, trans, phonetic, timedelta, timestamp FROM cards WHERE id=:id" );
    q.bindValue(":id", id);
    if ( q.exec() )
    {
        if ( q.next() )
        {
            card_id = id;
            checked = true;
            reversed = rand() % 2 == 1;
            phrase = q.value("phrase").toString();
            trans = q.value("trans").toString();
            phonetic = q.value("phonetic").toString();
            timedelta = q.value("timedelta").toInt();
            timestamp = q.value("timestamp").toString();
            getRandomExample();
            refreshExamplesModel();            
            emit currentCardUpdated();
            result = true;
         }
         else
         {
            qDebug() << q.lastError();
         }
    }
    else
    {
        qDebug() << q.lastError();
    }

    return result;
}

void MyObject::cardAdd( const QString& phrase, const QString& trans, const QString& phonetic )
{
    int id = card_add(phrase, trans, phonetic);
    if ( id > 0 ) {
        refreshCardsModel();
        cardOpen(id);
    }
}

void MyObject::cardUpdate(int id, const QString &phrase, const QString &trans, const QString &phonetic)
{
    QSqlQuery q;
    q.prepare("UPDATE cards SET phrase=:phrase, trans=:trans, phonetic=:phonetic WHERE id=:id");
    q.bindValue(":phrase", phrase);
    q.bindValue(":trans", trans);
    q.bindValue(":phonetic", phonetic);
    q.bindValue(":id", id);
    if ( q.exec() )
    {
        cardOpen(id);
    }
    else
    {
        qDebug() << q.lastError();
    }
}

void MyObject::cardNext()
{
    phrase.clear();
    trans.clear();
    phonetic.clear();
    example.clear();

    QSqlQuery q;

    static const char* sqls[] = {
        "SELECT id FROM cards WHERE id!=:id ORDER BY timestamp LIMIT 10",
        "SELECT id FROM cards WHERE id!=:id ORDER BY timedelta LIMIT 10",
    };

    q.prepare(sqls[getMode()]);
    q.bindValue(":id", card_id);
    if ( q.exec() )
    {
        QVector<int> ids;
        while ( q.next() )
        {
            ids.push_back(q.value("id").toInt());
        }
        if ( !ids.isEmpty() )
        {
            quint32 index = QRandomGenerator::global()->bounded(0, ids.size()-1);
            card_id = ids.at(index);
            q.prepare( "SELECT phrase, trans, phonetic, timedelta, timestamp FROM cards WHERE id=:id" );
            q.bindValue(":id", card_id);
            if ( q.exec() )
            {
                if ( q.next() )
                {
                    checked = false;
                    reversed = rand() % 2 == 1;
                    phrase = q.value("phrase").toString();
                    trans = q.value("trans").toString();
                    phonetic = q.value("phonetic").toString();
                    timedelta = q.value("timedelta").toInt();
                    timestamp = q.value("timestamp").toString();
                    getRandomExample();
                    refreshExamplesModel();
                }
            }
            else
            {
                qDebug() << q.lastError();
            }
        }
    }
    else
    {
        qDebug() << q.lastError();
    }

    emit currentCardUpdated();
}

void MyObject::cardSpeakPhrase()
{
    player->setSource(QUrl::fromLocalFile(::get_data_path() + QDir::separator() + phrase + ".mp3"));
    player->play();
}

void MyObject::exampleAdd(const QString &example)
{
    QStringList lines(example.split('\n'));
    foreach( const QString& str, lines ) {
        QString line(str.trimmed());
        if ( !line.isEmpty() ) {
            example_add( card_id, line );
        }
    }
    refreshExamplesModel();
}

void MyObject::exampleUpdate(int id, const QString &example)
{
    QSqlQuery q;
    q.prepare("UPDATE examples SET text=:text WHERE id=:id");
    q.bindValue(":text", example);
    q.bindValue(":id", id);
    if ( q.exec() )
    {
        refreshExamplesModel();
    }
    else
    {
        qDebug() << q.lastError();
    }
}

void MyObject::exampleRemove(int id)
{
    QSqlQuery q;
    q.prepare("DELETE FROM examples WHERE id=:id");
    q.bindValue(":id", id);
    if ( q.exec() )
    {
        refreshExamplesModel();
    }
    else
    {
        qDebug() << q.lastError();
    }
}

void MyObject::setCardsModelFilter(const QString &f)
{
    modelCards->setFilter(f);
    refreshCardsModel();
}

void MyObject::refreshCardsModel()
{
    modelCards->updateModel();
}

void MyObject::refreshExamplesModel()
{
    modelExamples->updateModel(card_id);
}

void MyObject::speak(const QString& locale, const QString& str)
{
#if defined(ANDROID)
    QAndroidJniObject l = QAndroidJniObject::fromString(locale);
    QAndroidJniObject s = QAndroidJniObject::fromString(str);
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/cards/MainActivity", "speak", "(Ljava/lang/String;Ljava/lang/String;)V", l.object<jstring>(), s.object<jstring>());
#else
    Q_UNUSED(locale)
    Q_UNUSED(str)
#endif
}

void MyObject::stateChanged(Qt::ApplicationState state)
{
    if ( Qt::ApplicationSuspended == state )
    {
        ini->sync();
    }
}

void MyObject::getRandomExample()
{
    QSqlQuery q;
    q.prepare( "SELECT text FROM examples WHERE card_id=:id" );
    q.bindValue(":id", card_id);
    if ( q.exec() )
    {
        QStringList examples;
        while ( q.next() )
        {
            examples.push_back(q.value("text").toString());
        }
        if ( examples.isEmpty() )
        {
            example.clear();
        }
        else if ( 1 == examples.size() )
        {
            example = examples.at(0);
        }
        else
        {
            example = examples.at(rand() % examples.size());
        }
    }
}
