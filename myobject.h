#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QObject>
#include <QMediaPlayer>

class QSettings;

class MyObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int mode READ getMode WRITE setMode NOTIFY modeChanged)

    QMediaPlayer* player;

    int card_id;
    QString phrase;
    QString trans;
    QString phonetic;
    QString example;
    QString timestamp;
    bool reversed;
    bool checked;
    int timedelta;

public:
    MyObject(QObject *parent = nullptr);

    int getMode() const;
    void setMode(int val);

    Q_INVOKABLE int currId() const;
    Q_INVOKABLE QString currPhrase() const;
    Q_INVOKABLE QString currTrans() const;
    Q_INVOKABLE QString currPhonetic() const;
    Q_INVOKABLE QString currExample() const;
    Q_INVOKABLE QString currTimestamp() const;
    Q_INVOKABLE bool currReversed() const;
    Q_INVOKABLE bool currChecked() const;
    Q_INVOKABLE bool currMediaExists() const;
    Q_INVOKABLE void currCheck();

    Q_INVOKABLE void cardAdd( const QString& phrase, const QString& trans, const QString& phonetic );
    Q_INVOKABLE void cardUpdate( int id, const QString& phrase, const QString& trans, const QString& phonetic );
    Q_INVOKABLE void cardRemove();
    Q_INVOKABLE void cardUpdate( bool success );
    Q_INVOKABLE bool cardOpen( int id );
    Q_INVOKABLE void cardNext();
    Q_INVOKABLE void cardSpeakPhrase();

    Q_INVOKABLE void exampleAdd( const QString& example );
    Q_INVOKABLE void exampleUpdate( int id, const QString& example );
    Q_INVOKABLE void exampleRemove( int id );

    Q_INVOKABLE void setCardsModelFilter( const QString& f );

    Q_INVOKABLE void refreshCardsModel();
    Q_INVOKABLE void refreshExamplesModel();

    void speak( const QString& locale, const QString& str );

private slots:
    void stateChanged(Qt::ApplicationState state);

signals:
    void currentCardUpdated();
    void modeChanged();

private:
    void getRandomExample();
};

#endif // MYOBJECT_H
