#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>

class Server : public QObject
{
    Q_OBJECT

    int card_id;
    int example_no;

    QTcpServer* server;

public:
    Server(QObject *parent = nullptr);

    Q_INVOKABLE void start();

signals:
    void message(QString txt);

private slots:
    void newConnection();
    void readyRead();
};

#endif // SERVER_H
