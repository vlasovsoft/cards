#ifndef DB_H
#define DB_H

class QString;
class QSqlDatabase;

bool initDatabase( QSqlDatabase& db );

int card_add( const QString& phrase, const QString& trans, const QString& phonetic );
void example_add ( int card_id, const QString& example );

#endif // DB_H
