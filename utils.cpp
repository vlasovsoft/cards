#include <QLocale>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>

#include "utils.h"

QString get_root()
{
#if defined(ANDROID)
    return "assets:";
#else
    return QCoreApplication::applicationDirPath();
#endif
}

QString get_qml_root()
{
#if defined(ANDROID)
    return "assets:";
#else
    return QString("file://") + QCoreApplication::applicationDirPath();
#endif
}

QString get_data_path()
{
#if defined(ANDROID)
    QString path(qgetenv("DB_PATH"));
#else
    QString path(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if ( !QDir( path ).exists() ) {
        QDir().mkpath( path );
    }
#endif
    return path;
}

QString get_lang()
{
    return QLocale::languageToString(QLocale::system().language());
}

int rand( int min, int max )
{
    return min + rand() % ( max - min + 1 );
}

