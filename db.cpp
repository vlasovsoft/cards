#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <QDebug>

#include "utils.h"

#include "db.h"

static bool isTableExists(const QString& name)
{
    bool result = false;
    QSqlQuery q;
    q.prepare("SELECT COUNT(1) FROM sqlite_master WHERE name=:name");
    q.bindValue(":name", name);
    if ( q.exec() )
    {
        q.next();
        result = q.value(0).toInt() > 0;
    }
    else
    {
        qDebug() << q.lastError();
    }
    return result;
}

static void createTableCards()
{
    QSqlQuery q;
    if ( !q.exec( "CREATE TABLE cards ( "
                  "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                  "phrase TEXT,"
                  "trans TEXT,"
                  "phonetic TEXT,"
                  "timestamp DATETIME DEFAULT (datetime('now','localtime')), "
                  "timedelta INTEGER NOT NULL DEFAULT 60 )"
                  ) )
    {
        qDebug() << q.lastError();
    }
}

static void createTableExamples()
{
    QSqlQuery q;
    if ( !q.exec( "CREATE TABLE examples ( "
                  " id INTEGER PRIMARY KEY AUTOINCREMENT, "
                  " card_id INTEGER, "
                  " text TEXT, "
                  " CONSTRAINT fk_card "
                  " FOREIGN KEY (card_id) "
                  " REFERENCES cards(id) "
                  " ON DELETE CASCADE"
                  " ) "
                  ) )
    {
        qDebug() << q.lastError();
    }
}

static void createTableProperties()
{
    QSqlQuery q;
    if ( !q.exec( "CREATE TABLE properties ( "
                  "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                  "name TEXT,"
                  "value TEXT ) "
                   ) )
    {
        qDebug() << q.lastError();
    }
}

bool initDatabase( QSqlDatabase& db )
{
    QString path(::get_data_path());
    db.setDatabaseName( path + QDir::separator() + "cards.db" );
    if ( db.open() )
    {
        if ( !isTableExists( "cards" ) )
            createTableCards();
        if ( !isTableExists("examples") )
            createTableExamples();
        if ( !isTableExists( "properties" ) )
            createTableProperties();

        return true;
    }

    return false;
}

int card_add(const QString &phrase, const QString &trans, const QString &phonetic)
{
    int result = 0;

    QSqlQuery q;
    q.prepare("SELECT COUNT(1) FROM cards WHERE phrase=:phrase AND trans=:trans");
    q.bindValue(":phrase", phrase);
    q.bindValue(":trans", trans);
    if ( !q.exec() )
    {
        qDebug() << q.lastError();
    }
    else
    {
        q.next();
        int count = q.value(0).toInt();
        if ( 0 == count ) {
            q.prepare("INSERT INTO cards(phrase, trans, phonetic) VALUES (:phrase, :trans, :phonetic)");
            q.bindValue(":phrase", phrase);
            q.bindValue(":trans", trans);
            q.bindValue(":phonetic", phonetic);
            if ( q.exec() )
            {
                result = q.lastInsertId().toInt();
            }
            else
            {
                qDebug() << q.lastError();
            }
        }
    }

    return result;
}

void example_add(int card_id, const QString &example)
{
    QSqlQuery q;
    q.prepare("INSERT INTO examples(card_id, text) VALUES (:card_id, :text)");
    q.bindValue(":card_id", card_id);
    q.bindValue(":text", example);
    q.exec();
}
