#include <QTcpSocket>

#include "db.h"

#include "server.h"

Server::Server(QObject *parent)
    : QObject(parent)
    , card_id(0)
    , example_no(0)
    , server(new QTcpServer(this))
{
    QObject::connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));
}

void Server::start()
{
    if ( server->listen(QHostAddress::Any, 4444) )
    {
        emit message(QString("Server atarted. Host: %1 Port: %2").arg(server->serverAddress().toString()).arg(server->serverPort()));
    }
}

void Server::newConnection()
{
    // need to grab the socket
    QTcpSocket *socket = server->nextPendingConnection();
    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void Server::readyRead()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if ( socket ) {
        while ( socket->canReadLine() )
        {
            QString line = QString::fromUtf8(socket->readLine()).trimmed();
            if ( !line.isEmpty() ) {
                QStringList parts = line.split('|');
                if ( 3 == parts.size() )
                {
                    // new phrase
                    card_id = card_add(parts.at(0),parts.at(2),parts.at(1));
                    if ( card_id > 0 )
                    {
                        example_no = 0;
                        emit message(line);
                    }
                }
                else
                if ( card_id > 0 )
                {
                    // example to the current phrase
                    example_add(card_id, line);
                    emit message(QString(" %1. %2").arg(example_no+1).arg(line));
                    example_no++;
                }
            }
        }
    }
}
