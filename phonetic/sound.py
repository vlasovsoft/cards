#!/usr/bin/python3
import os
import argparse
import datetime
import requests
from bs4 import BeautifulSoup
from bs4 import Comment
import sqlite3

URL1 = "https://dictionary.cambridge.org"
URL2 = URL1 + "/dictionary/english/"

conn = sqlite3.connect('cards.db')
c = conn.cursor()

for row in c.execute('SELECT phrase FROM cards'):
    phrase = row[0]
    file_name = phrase + '.mp3'
    if not os.path.exists(file_name):
        print(file_name)
        r = requests.get(url=URL2+"/"+phrase)
        d = BeautifulSoup(r.text, 'html.parser')
        for link in d.find_all('source', attrs={"type": "audio/mpeg"}):
            src = link.get('src')
            if "/uk_pron/" in src:
                r1 = requests.get(URL1+src)
                with open(phrase+'.mp3', 'wb') as f:
                    f.write(r1.content)
            break

conn.close()

