#include "catslistmodel.h"

CatsListModel::CatsListModel(QObject *parent)
    : QSqlQueryModel(parent)
{
    updateModel();
}

QVariant CatsListModel::data(const QModelIndex &index, int role) const
{
    // Define the column number, on the role of number
    int columnId = role - Qt::UserRole - 1;

    // Create the index using a column ID
    QModelIndex modelIndex = this->index(index.row(), columnId);

    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

QHash<int, QByteArray> CatsListModel::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[CategoryRole] = "category";
    return roles;
}

void CatsListModel::updateModel()
{
    setQuery("SELECT id, category FROM categories");
}

int CatsListModel::getId(int row)
{
    return this->data(this->index(row, 0), IdRole).toInt();
}
