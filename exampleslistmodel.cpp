#include "exampleslistmodel.h"

ExamplesListModel::ExamplesListModel(QObject *parent)
    : QSqlQueryModel(parent)
{
    updateModel(0);
}

QVariant ExamplesListModel::data(const QModelIndex &index, int role) const
{
    // Define the column number, on the role of number
    int columnId = role - Qt::UserRole - 1;

    // Create the index using a column ID
    QModelIndex modelIndex = this->index(index.row(), columnId);

    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

QHash<int, QByteArray> ExamplesListModel::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[TextRole] = "modeltext";
    return roles;
}

void ExamplesListModel::updateModel(int cardId)
{
    setQuery(QString("SELECT id, text FROM examples WHERE card_id = %1").arg(cardId));
}

int ExamplesListModel::getId(int row)
{
    return this->data(this->index(row, 0), IdRole).toInt();
}

QString ExamplesListModel::getExample(int row) const
{
    return data(index(row,0), TextRole).toString();
}
